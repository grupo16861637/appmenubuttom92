import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.appmenubuttom92.AcercaFragment
import com.example.appmenubuttom92.Database.Alumno
import com.example.appmenubuttom92.DbFragment
import com.example.appmenubuttom92.R
import java.util.Locale

class MiAdaptador(
    private var listaAlumnos: ArrayList<Alumno>,
    private val context: Context
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>() {

    private val listaAlumnosFull: ArrayList<Alumno> = ArrayList(listaAlumnos)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.alumn_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.txtMatricula.text = "Matricula: ${alumno.matricula}"
        holder.txtNombre.text = "Nombre: ${alumno.nombre}"
        holder.txtCarrera.text = "Especialidad: ${alumno.especialidad}"

        holder.txtNombre.setTextColor(context.resources.getColor(android.R.color.white))
        holder.txtCarrera.setTextColor(context.resources.getColor(android.R.color.white))
        holder.txtMatricula.setTextColor(context.resources.getColor(android.R.color.white))

        Glide.with(context)
            .load(alumno.foto)
            .apply(RequestOptions().override(100, 100))
            .into(holder.idImagen)

        holder.itemView.setOnClickListener {
            val fragment = DbFragment()
            val bundle = Bundle()
            bundle.putParcelable("alumno", alumno)
            fragment.arguments = bundle

            val activity = context as AppCompatActivity
            val currentFragment = activity.supportFragmentManager.findFragmentById(R.id.ac)

            activity.supportFragmentManager.beginTransaction().apply {
                currentFragment?.let {
                    if (it.isVisible) {
                        hide(it)
                    }
                }
                replace(R.id.ac, fragment)
                addToBackStack(null)
                commit()
            }

            val agregarAlumnoBtn = activity.findViewById<com.google.android.material.floatingactionbutton.FloatingActionButton>(R.id.agregarAlumno)
            agregarAlumnoBtn.hide()
        }
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun filter(query: String) {
        val filteredList = if (query.isEmpty()) {
            listaAlumnosFull
        } else {
            val filterPattern = query.toLowerCase(Locale.getDefault()).trim()
            val words = filterPattern.split("\\s+".toRegex())

            listaAlumnosFull.filter { alumno ->
                val fullName = alumno.nombre.toLowerCase(Locale.getDefault())
                words.all { word -> fullName.contains(word) }
            }
        }

        listaAlumnos = ArrayList(filteredList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre: TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        val txtMatricula: TextView = itemView.findViewById(R.id.txtMatricula)
        val txtCarrera: TextView = itemView.findViewById(R.id.txtCarrera)
        val idImagen: ImageView = itemView.findViewById(R.id.foto)
    }
}

