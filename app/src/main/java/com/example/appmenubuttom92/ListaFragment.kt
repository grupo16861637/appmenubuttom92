package com.example.appmenubuttom92

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import android.widget.Filter
import java.util.*
import kotlin.collections.ArrayList

class ListaFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: CustomArrayAdapter
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate el diseño para este fragmento
        val view = inflater.inflate(R.layout.fragment_lista, container, false)

        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.searchView)

        val items = resources.getStringArray(R.array.alumnos)

        arrayList = ArrayList()
        arrayList.addAll(items)

        adapter = CustomArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, arrayList)

        listView.adapter = adapter

        listView.setOnItemClickListener { parent, view, position, id ->
            val alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage("$position : $alumno")
            builder.setPositiveButton("OK") { dialog, which ->
            }
            builder.show()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })

        return view
    }

    private class CustomArrayAdapter(context: Context, resource: Int, objects: List<String>) :
        ArrayAdapter<String>(context, resource, objects) {
        private var originalList: List<String> = objects
        private var filteredList: List<String> = objects

        override fun getCount(): Int {
            return filteredList.size
        }

        override fun getItem(position: Int): String? {
            return filteredList[position]
        }

        override fun getFilter(): Filter {
            return object : Filter() {
                override fun performFiltering(constraint: CharSequence?): FilterResults {
                    val filterResults = FilterResults()
                    if (constraint.isNullOrEmpty()) {
                        filterResults.count = originalList.size
                        filterResults.values = originalList
                    } else {
                        val filterPattern = constraint.toString().toLowerCase(Locale.getDefault()).trim()
                        val words = filterPattern.split("\\s+".toRegex())

                        val filteredResults = originalList.filter { item ->
                            val fullName = item.toLowerCase(Locale.getDefault())
                            // Verificar que cada palabra del patrón esté presente en cualquier parte del nombre
                            words.all { word -> fullName.contains(word) }
                        }
                        filterResults.count = filteredResults.size
                        filterResults.values = filteredResults
                    }
                    return filterResults
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    filteredList = results?.values as List<String>
                    notifyDataSetChanged()
                }
            }
        }
    }
}
